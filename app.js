require('newrelic');
require('dotenv').config();
const { ApolloServer } = require('apollo-server');
const typeDefs         = require('./graphql/typeDefs/typeDefs');
const resolvers        = require('./graphql/resolvers/index');

const server = new ApolloServer({
    cors: {
        origin: '*', // allow request from all domains
        credentials: true
    },
    typeDefs,
    resolvers,
    context: ({ req, res }) => {        
        let data = {};

        // BEGIN AUTH
        const authorizationHeader = req.get('Authorization');
        if (!authorizationHeader) {
            data.isUserAuthorized = false; // no header provided
            return { data };
        }
        
        const token = authorizationHeader.split(' ')[1];
        if (!token || token === '') { // auth header is invalid
            data.isUserAuthorized = false;
            return { data };
        }
        
        try {
            let decodedToken = jwt.verify(token, 'somesupersecretkey');
            if (!decodedToken) { // cannot verify json web token, invalid token
                data.isUserAuthorized = false;
                return { data };
            }
        } catch (error) { // cannot verify json web token, invalid token
            data.isUserAuthorized = false;
            return { data };
        }
        
        // user is authorized
        data.isUserAuthorized = true;
        // END AUTH
        
        return { data };
    },
    engine: {
        apiKey: process.env.ENGINE_API_KEY
    },
    introspection: true
});

server.listen({ port: process.env.PORT }).then(({ url }) => { console.log(`Server running at ${url}`); });
