const authenticationResolvers = require('./authenticationResolvers');

// this is the root resolver
// you can pass more resolvers into this object to add them to the root resolver
const resolvers = {
  ...authenticationResolvers
};

module.exports = resolvers;
