const bcrypt = require('bcryptjs');
const jwt    = require('jsonwebtoken');

const GraphQLJSON = require('graphql-type-json');

// BEGIN FIREBASE
const firebase     = require('firebase-admin');
let serviceAccount = require('../../firebase/serviceAccountKey.json');

firebase.initializeApp({
    credential: firebase.credential.cert(serviceAccount)
});

let db            = firebase.firestore();
let user_accounts = db.collection('user_accounts');
// END FIREBASE

const authenticationResolvers = {
    Query: {
        getExistingUser: (parent, args, context, info) => {
            async function getExistingUser() {
                let user = await user_accounts.doc(args.email).get();
                    user = user.data();

                if(!user) {
                    return "There is an issue with either your email or your password!";
                }

                user_accounts.doc(args.email).update({                    
                    clientMetadataMostRecent: args.clientMetadata,
                });

                const bcrpytComparePasswordResult = await bcrypt.compare(args.password, user.password);
                if(bcrpytComparePasswordResult == true) {
                    return jwt.sign({ email: user.email }, process.env.JWTSecretKey, {expiresIn: '24h'});
                } else {
                    return "There is an issue with either your email or your password!";
                }
            }
            return getExistingUser();
        }
    },
    Mutation: {
        createNewUser: (parent, args, context, info) => {
            async function createNewUser() {
                let existingUser = await user_accounts.doc(args.email).get();
                if(existingUser.exists) {
                    return "This email is already registered.";
                }
                
                try {
                    // create user document
                    user_accounts.doc(args.email).set({                    
                        email: args.email,
                        password: await bcrypt.hash(args.password, 12),
                        clientMetadataAccountCreation: args.clientMetadata,
                    });

                    return await jwt.sign({ email: args.email }, process.env.JWTSecretKey, {expiresIn: '24h'});
                } catch (error) {
                    return "There was an error saving the new user to the database. " + error;
                }
            }
            return createNewUser();
        }
    },
    JSON: GraphQLJSON,
};

module.exports = authenticationResolvers;
