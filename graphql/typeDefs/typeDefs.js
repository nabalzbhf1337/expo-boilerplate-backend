const { gql } = require('apollo-server');

const typeDefs = gql`
  scalar JSON

  type Query {
    getExistingUser(email: String password: String clientMetadata: JSON): String
    addWindowObjectToUser(email: String! object: JSON!): String
  }

  type Mutation {
    createNewUser(email: String password: String ipAddress: String clientMetadata: JSON): String
  }
`;

module.exports = typeDefs;
